/**
 * Module dependencies.
 */
var express = require('express')
  , user = require('./routes/users_route')
  , http = require('http')
  , path = require('path')
  , fs = require('fs')
  , mongoose = require('mongoose')
  , PropertiesReader = require('properties-reader');

var app = express();
var session = require('express-session');
var bodyParser = require('body-parser');
var expressJwt = require('express-jwt');

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/public/views');
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, '/public/views')));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(session({ secret: "RANDOM STRING FOR TEST", resave: false, saveUninitialized: true }));

 // use JWT auth to secure the api
 app.use('/api', expressJwt({ secret: "RANDOM STRING FOR TEST" }).unless({ path: ['/api/users/authenticate', '/api/users/register'] }));

 //routes
 app.use('/login', require('./routes/login.controller'));
 app.use('/register', require('./routes/register.controller'));
app.use('/about', require('./routes/about.controller'));
 app.use('/app', require('./routes/app.controller'));
 app.use('/device', require('./routes/deviceController'));
app.use('/userDetails', require('./routes/userController'));


var properties = PropertiesReader('./configurations.properties');

// development only
if ('development' == app.get('env')) {
  //app.use(express.errorHandler());
  mongoose.connect(properties.get('mongoLabUri'));
    var db = mongoose.connection;
    db.on('error', function callback() {
        console.log(' DB Connection Error');
    });
    db.on('connected', function callback() {
        console.log(' DB Connection Connected');
    });
    db.on('disconnected', function callback() {
        console.log(' DB Connection Disconnected');
    });
    db.on('close', function callback() {
        console.log(' DB Connection Closed');
    });
    db.on('reconnected', function callback() {
        console.log(' DB Connection Reconnected');
    });
    db.on('fullsetup', function callback() {
        console.log(' DB Connection Full Setup');
    });
    db.on('open', function callback() {
        console.log(' DB Connection Open');
    });
}

//load all files in model directory
fs.readdirSync(__dirname + '/models').forEach(function(filename){
	if(~filename.indexOf('.js')) require(__dirname + '/models/'+filename)
});

// make '/app' default route
app.get('/', function (req, res) {
    return res.redirect('/app');
});
//app.get('/', routes.index);
//app.get('/users',user.getAllUsers);
app.post('/signIn',user.signIn);
app.post('/users',user.register);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});