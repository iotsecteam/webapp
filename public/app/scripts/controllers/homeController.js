/**
 * New node file
 */
(function () {
    'use strict';

    angular
        .module('app')
        .controller('HomeController', Controller).controller('DeviceController', deviceController);

    function deviceController($scope, $element, UserService, DeviceService, $rootScope) {
        $scope.addDevice = function () {
            $scope.device.userId = UserService.getCurrentUser();
            DeviceService.addDevice($scope.device).success(function (data, status) {
                $rootScope.$broadcast("DeviceAdded");
            }).error(function (data, status) {
            });
            $element.modal('hide');
        };
    }

    function Controller($scope, $element, UserService, ModalService, DeviceService) {
        var vm = this;

        vm.user = null;

        function initController() {
            // get current user
            UserService.GetCurrent().then(function (user) {
                vm.user = user;
            });
        }

        var hackedDevices=[];
        var disabledDevices=[];
        var addToHackedDevice = function(device_id) {
            hackedDevices.push(device_id);
        };
        var addToDisabledDevice = function(device_id) {
            disabledDevices.push(device_id);
        };

        var markHackedDevices = function(){

            for(var i=0;i<hackedDevices.length;i++){

                deviceInDanger(hackedDevices[i]);
            }

            for(var i=0;i<disabledDevices.length;i++){

                deviceDetached(disabledDevices[i]);
            }
        };

        var displayDeviceDetails = function () {
            $scope.showme = true;

            DeviceService.getDeviceDetails().success(function (data, status) {
                var userId = data.userId;
                UserService.setCredentials(userId);
                var isSuccess = data.isSuccess;
                if (isSuccess) {
                    var devices = data.deviceData;

                    for(var i=0;i<devices.length;i++){
                        if(devices[i].status==0) {
                            connectBroker(devices[i].device_id);
                        }
                        else if(devices[i].status==1){
                            addToHackedDevice(devices[i].device_id);
                        }
                        else if(devices[i].status==2){
                            addToDisabledDevice(devices[i].device_id);
                        }
                    }
                    $scope.devices = devices;
                    markHackedDevices();
                }
            }).error(function (data, status) {
            });
        };

        displayDeviceDetails();


        $scope.$on("DeviceAdded", function(event, args){
           displayDeviceDetails();
        });

        $scope.displayModal = function () {
            ModalService.showModal({
                templateUrl: "app/views/deviceRegistration.html",
                controller: "DeviceController",
                backdrop: 'static'
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                });
            });
        };

        $scope.removeDevice = function (item) {
            var btn = "btn"+item.device_id;
            var btnClass = document.getElementById(btn).className;
            if(btnClass == "btn btn-sm btn-success btn-circle") {
                DeviceService.removeDevice(item).success(function (data, status) {
                    displayDeviceDetails();
                }).error(function (data, status) {
                    //show some error
                });
            }
        };

        $scope.close = function () {
            close({}, 500); // close, but give 500ms for bootstrap to animate
        };

        $scope.initConfig = function (config, item) {
            console.log("initConfig " + item.device_id);
        };

        $scope.detachDevice= function(btnId){
            var btn= "btn"+btnId;
            if(document.getElementById(btn).className=="btn btn-sm btn-danger btn-circle"){
                var client = mqtt.connect({host: 'test.mosquitto.org', port: 8080});
                var publishObject = {};
                publishObject["device_id"] = btnId;
                publishObject["setStatus"] = "disable";
                var PublishJSON = JSON.stringify(publishObject);
                client.publish('iotSecurity/deviceState', PublishJSON);
                updateStatus(2, btnId);
                deviceDetached(btnId);
            }
            if(document.getElementById(btn).className=="btn btn-sm btn-warning btn-circle") {
                var client = mqtt.connect({host: 'test.mosquitto.org', port: 8080});
                var publishObject = {};
                publishObject["device_id"] = btnId;
                publishObject["setStatus"] = "enable";
                var PublishJSON = JSON.stringify(publishObject);
                client.publish('iotSecurity/deviceState', PublishJSON);
                updateStatus(0, btnId);
                deviceAttached(btnId);
            }
        };

        var myChart;
        $scope.showTemp = function (item) {
            $scope.name="Device : " + item.device_name;
            var btn = "btn"+item.device_id;
            var btnClass = document.getElementById(btn).className;
            if(btnClass != "btn btn-sm btn-warning btn-circle") {
                connectBroker(item.device_id);
                $scope.showme = false;
                myChart = Highcharts.chart('4321t', {
                    chart: {
                        type: 'gauge',
                        plotBackgroundColor: null,
                        plotBackgroundImage: null,
                        plotBorderWidth: 0,
                        plotShadow: false
                    },

                    title: {
                        text: 'Temperature'
                    },

                    pane: {
                        startAngle: -150,
                        endAngle: 150,
                        background: [{
                            backgroundColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [
                                    [0, '#FFF'],
                                    [1, '#333']
                                ]
                            },
                            borderWidth: 0,
                            outerRadius: '109%'
                        }, {
                            backgroundColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [
                                    [0, '#333'],
                                    [1, '#FFF']
                                ]
                            },
                            borderWidth: 1,
                            outerRadius: '107%'
                        }, {
                            // default background
                        }, {
                            backgroundColor: '#DDD',
                            borderWidth: 0,
                            outerRadius: '105%',
                            innerRadius: '103%'
                        }]
                    },

                    // the value axis
                    yAxis: {
                        min: 10,
                        max: 40,

                        minorTickInterval: 'auto',
                        minorTickWidth: 1,
                        minorTickLength: 10,
                        minorTickPosition: 'inside',
                        minorTickColor: '#666',

                        tickPixelInterval: 30,
                        tickWidth: 2,
                        tickPosition: 'inside',
                        tickLength: 10,
                        tickColor: '#666',
                        labels: {
                            step: 2,
                            rotation: 'auto'
                        },
                        title: {
                            text: 'degrees'
                        },
                        plotBands: [{
                            from: 10,
                            to: 20,
                            color: '#55BF3B' // green
                        }, {
                            from: 20,
                            to: 30,
                            color: '#DDDF0D' // yellow
                        }, {
                            from: 30,
                            to: 40,
                            color: '#DF5353' // red
                        }]
                    },

                    series: [{
                        name: 'Temp',
                        data: [80],
                        tooltip: {
                            valueSuffix: ' degrees'
                        }
                    }]

                });
            }
        };


        var myChart2;
        $scope.showHumid = function (item) {

            var btn = "btn"+item.device_id;
            var btnClass = document.getElementById(btn).className;
            if(btnClass != "btn btn-sm btn-warning btn-circle") {
                myChart2 = Highcharts.chart('4321h', {
                    //$scope.chartConfig = {
                    chart: {
                        type: 'gauge',
                        plotBackgroundColor: null,
                        plotBackgroundImage: null,
                        plotBorderWidth: 0,
                        plotShadow: false
                    },

                    title: {
                        text: 'Humidity'
                    },

                    pane: {
                        startAngle: -150,
                        endAngle: 150,
                        background: [{
                            backgroundColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [
                                    [0, '#FFF'],
                                    [1, '#333']
                                ]
                            },
                            borderWidth: 0,
                            outerRadius: '109%'
                        }, {
                            backgroundColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [
                                    [0, '#333'],
                                    [1, '#FFF']
                                ]
                            },
                            borderWidth: 1,
                            outerRadius: '107%'
                        }, {
                            // default background
                        }, {
                            backgroundColor: '#DDD',
                            borderWidth: 0,
                            outerRadius: '105%',
                            innerRadius: '103%'
                        }]
                    },

                    // the value axis
                    yAxis: {
                        min: 20,
                        max: 50,

                        minorTickInterval: 'auto',
                        minorTickWidth: 1,
                        minorTickLength: 10,
                        minorTickPosition: 'inside',
                        minorTickColor: '#666',

                        tickPixelInterval: 30,
                        tickWidth: 2,
                        tickPosition: 'inside',
                        tickLength: 10,
                        tickColor: '#666',
                        labels: {
                            step: 2,
                            rotation: 'auto'
                        },
                        title: {
                            text: 'degrees'
                        },
                        plotBands: [{
                            from: 20,
                            to: 30,
                            color: '#55BF3B' // green
                        }, {
                            from: 30,
                            to: 40,
                            color: '#DDDF0D' // yellow
                        }, {
                            from: 40,
                            to: 50,
                            color: '#DF5353' // red
                        }]
                    },

                    series: [{
                        name: 'Humidity',
                        data: [80],
                        tooltip: {
                            valueSuffix: ' %'
                        }
                    }]

                });
            }
        };

       var updateStatus = function(newStatus, deviceId){
           var device = {};
           device.deviceId = deviceId;
           device.status = newStatus;
           DeviceService.changeStatus(device).success(function (data, status) {
           }).error(function (data, status) {
               //show some error
           });
       }


        var deviceInDanger = function(deviceId){
            var tableRowId = "tr"+deviceId;
            var btnId = "btn"+deviceId;
            var linkId = "i"+deviceId;
            var s = "showDevice" + deviceId;
            var remove = "remove" + deviceId;
           // alert("Device "+deviceId+ " in danger "+tableRowId);
            setTimeout(function(){
                document.getElementById(tableRowId).style.backgroundColor="#F75D59";
                document.getElementById(btnId).className = "btn btn-sm btn-danger btn-circle";
                document.getElementById(linkId).className = "fa fa-close";
                document.getElementById(btnId).title="Device hacked";
                document.getElementById(remove).title = "Can not remove";
                $scope.c=true;
            }, 1000);
        };

        var deviceDetached = function(deviceId){
            var tableRowId = "tr"+deviceId;
            var btnId = "btn"+deviceId;
            var linkId = "i"+deviceId;
            var s = "showDevice" + deviceId;
            var remove = "remove" + deviceId;
            // alert("Device "+deviceId+ " in danger "+tableRowId);
            setTimeout(function(){
                document.getElementById(tableRowId).style.backgroundColor="silver";
                document.getElementById(btnId).className = "btn btn-sm btn-warning btn-circle";
                document.getElementById(linkId).className = "fa fa-undo";
                document.getElementById(btnId).title="Device disabled";
                document.getElementById(remove).title = "Can not remove";
                $scope.c=true;
            }, 1000);
        };

        var deviceAttached = function(deviceId){
            var tableRowId = "tr"+deviceId;
            var btnId = "btn"+deviceId;
            var linkId = "i"+deviceId;
            var s = "showDevice" + deviceId;
            var remove = "remove" + deviceId;
            // alert("Device "+deviceId+ " in danger "+tableRowId);
            setTimeout(function(){
                document.getElementById(tableRowId).style.backgroundColor="white";
                document.getElementById(btnId).className = "btn btn-sm btn-success btn-circle";
                document.getElementById(linkId).className = "fa fa-check";
                document.getElementById(btnId).title="Safe!";
                document.getElementById(remove).title = "Remove";
                $scope.c=true;
            }, 1000);
        };

        var connectBroker = function (currentID) {
            var client = mqtt.connect({host: 'test.mosquitto.org', port: 8080}); // you add a ws:// url here
            //var client = mqtt.connect({host: '192.168.0.21', port:9000});
            client.subscribe("iotSecurity/" + currentID);
            client.subscribe("iotSecurity/deviceHacked");
            client.on("message", function (topic, payload) {
                if(topic=="iotSecurity/deviceHacked"){
                    var json = JSON.parse(payload);
                    deviceInDanger(json.device_id);
                    var deviceId = json.device_id;
                    //update status in DB
                    updateStatus(1, deviceId);
                }
                else {
                    var json = JSON.parse(payload);
                    var temp = json.temperature;
                    var hum = json.humidity;
                    var point = myChart.series[0].points[0];
                    point.update(Number(temp));
                    var point2 = myChart2.series[0].points[0];
                    point2.update(Number(hum));
                }
            });
        }

    }

})();


