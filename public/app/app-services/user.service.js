/**
 * New node file
 */
(function () {
    'use strict';
 
    angular
        .module('app')
        .factory('UserService', Service);
 
    function Service($http, $q, $rootScope) {
        var service = {};
 
        service.GetCurrent = GetCurrent;
        service.GetAll = GetAll;
        service.GetById = GetById;
        service.GetByUsername = GetByUsername;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
        service.setCredentials = setCredentials;
        service.clearCredentials = clearCredentials;
        service.getCurrentUser = getCurrentUser;
 
        return service;
 
        function GetCurrent(userId) {
            return $http.get('/userDetails/getCurrentUser/'+userId);
        }

        function GetAll() {
            return $http.get('/api/users').then(handleSuccess, handleError);
        }

        function GetById(_id) {
            return $http.get('/api/users/' + _id).then(handleSuccess, handleError);
        }
 
        function GetByUsername(username) {
            return $http.get('/api/users/' + username).then(handleSuccess, handleError);
        }
 
        function Create(user) {
            return $http.post('/api/users', user).then(handleSuccess, handleError);
        }
 
        function Update(user) {
            return $http.post('/userDetails/updateUser', user);
        }
 
        function Delete(_id) {
            return $http.post('/userDetails/deleteUser', user);
        }
 
        // private functions
 
        function handleSuccess(res) {
            return res.data;
        }
 
        function handleError(res) {
            return $q.reject(res.data);
        }




    function setCredentials(userId) {
            $rootScope.currentUserId = userId;

        };
         function clearCredentials() {
            $rootScope.currentUserId = "";

        };
        function getCurrentUser(){
            return $rootScope.currentUserId;
        };
    }


 
})();