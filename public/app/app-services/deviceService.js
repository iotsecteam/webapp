(function () {
    'use strict';

    angular
        .module('app')
        .factory('DeviceService', Service);

    function Service($http, $q) {
        var service = {};

        service.getDeviceDetails = getDeviceDetails;
        service.addDevice = addDevice;
        service.removeDevice = removeDevice;
        service.changeStatus = changeStatus;

        return service;

        function getDeviceDetails() {
            return $http.get('/device/getDeviceList');
        }

        function addDevice(device){
            return $http.post('/device/addDevice', device);
        }

        function removeDevice(device){
            return $http.post('/device/removeDevice', device);
        }

        function changeStatus(device){
            console.log("in device service");
            return $http.post('/device/changeStatus', device);
        }
    }
})();