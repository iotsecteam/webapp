﻿
var mongo = require('mongodb');
var monk = require('monk');
var db = monk(config.connectionString);
var usersDb = db.get('users');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var Q = require('q');

var mongoose = require('mongoose');
var crypto = require('crypto');

var service = {};

service.authenticate = authenticate;

module.exports = service;

function authenticate(username, password) {
    var deferred = Q.defer();

    usersDb.findOne({ username: username }, function (err, user) {
        if (err) deferred.reject(err);

        if (user && bcrypt.compareSync(password, user.hash)) {
            // authentication successful
            deferred.resolve(jwt.sign({ sub: user._id }, "RANDOM STRING FOR TEST"));
        } else {
            // authentication failed
            deferred.resolve();
        }
    });

    return deferred.promise;
}

exports.signIn = function(req, res) {
    var userModel = mongoose.model('users');
    userModel.find({
        userId : req.param("userId"),
        password : crypto.createHash("sha1").update(req.param("password"))
            .digest('hex')
    }, (function(err, users) {
        if (err)
            throw err;

        if (!users.length) {
            res.send("Invalid user", 200);
        } else
            res.send(users[0].name, 200);
    }))
};
