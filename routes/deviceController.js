var express = require('express');
var router = express.Router();
var request = require('request');
var DeviceModel = require('../models/device');

router.get('/getDeviceList', function (req, res) {
    var currentUser = req.session.currentUserId;

    Device.find({userId: currentUser}, function(err, docs){
        var responseData = {
          userId: currentUser
        };
       if(!err && docs.length > 0){
           responseData.isSuccess= true;
           var devices =[];
           for(var i=docs.length-1;i>=0;i--){
               var result=docs[i];
               var device ={
                   "_id": result._id,
                   "device_id": result.id,
                   "device_name": result.name,
                   "topic_name": result.deviceTopic,
                   "status" : result.status
               };
               devices.push(device);
           }

           responseData.deviceData = devices;
           res.json(responseData);
       }else{
           responseData.isSuccess = false;
           res.json(responseData);
       }
    });

});
var Device = new DeviceModel();
router.post('/addDevice', function (req, res) {
    var deviceId = req.body.id;
    var deviceName = req.body.name;
    var userId = req.body.userId;
    var status = 0;
    Device.create({
        id: deviceId,
        name: deviceName,
        deviceTopic: "iotSecurity/"+deviceId,
        userId: userId,
        status: status
    }, function(err, doc){
        if(!err){
            res.sendStatus(200);
        }else{
            res.sendStatus(400);
        }
    });
});
util = require("util");

router.post('/removeDevice',function(req, res){
    var deviceId = req.body.device_id;
    Device.remove({id: deviceId}, function(err){
        if(!err){
            res.sendStatus(200);
        }else{
            res.sendStatus(400);
        }
    });
});

router.post('/changeStatus', function(req, res){


});

module.exports = router;