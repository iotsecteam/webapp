﻿var express = require('express');
var router = express.Router();
var request = require('request');
var userRoute = require('./users_route');


router.get('/', function (req, res) {
    res.render('register');
});

router.post('/', function (req, res) {
    // register using api to maintain clean separation between layers

    userRoute.register(req, res, function(error, message){
        if(error){
            console.log("Register has an error.");
            return res.render('register', { error: 'An error occurred' });
        }

        if(message != ''){
            return res.render('register', {
                error: "User already exists!"
            });
        }
        else{
            req.session.success = 'Registration successful';
            return res.redirect('/login');
        }
    });

});

module.exports = router;