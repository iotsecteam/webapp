var mongoose = require('mongoose');
var crypto = require('crypto');
var _ = require('lodash');
var jwt = require('jsonwebtoken');

exports.getAllUsers = function(req, res) {
	var userModel = mongoose.model('users');
	mongoose.model('users').find(function(err, users) {
		res.send(users);
	})
};

exports.signIn = function(req, res, cb) {
	var userModel = mongoose.model('users');
    var token = "";
    var user_id = req.param("userId");
    var encryptedPassword = crypto.createHash("sha1").update(req.param("password"))
        .digest('hex');

	userModel.find({
		userId : user_id,
		password : encryptedPassword
    }, (function(err, user) {
		if (err)
            cb(err, token);

		if (!user.length) {
			cb(null, token);
		} else{
            token = jwt.sign({ sub: user._id }, "RANDOM STRING FOR TEST");
            req.session.currentUserId = user_id;
            cb(null, token);
        }
	}))
};

exports.register = function(req, res, cb) {
    var returnMessage = "";
	var userModel = mongoose.model('users');

	userModel.find({
		userId : req.param("userId")
	}, (function(err, users) {
		if (err)
			cb(err, returnMessage);

		if (!users.length) {
			// create a new user
			var newUser = new userModel({
				userId : req.param("userId"),
				password : req.param("password"),
				name : req.param("name"),
				gender : req.param("gender"),
				age : req.param("age")
			});

			newUser.encryptPassword(function(err, password) {
				if (err)
                    cb(err,returnMessage);
			});

			// call the built-in save method to save to the database
			newUser.save(function(err) {
				if (err)
                    cb(err, returnMessage);
				else{
                    cb(null, returnMessage);
                }

			});
		} else{
            returnMessage = "User already exists!";
            cb(null, returnMessage);
        }
	}));
	

};