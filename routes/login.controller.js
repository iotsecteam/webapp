﻿var express = require('express');
var router = express.Router();
var request = require('request');
var userRoute = require('./users_route');

router.get('/', function (req, res) {
    // log user out
    delete req.session.token;

    // move success message into local variable so it only appears once (single read)
    var viewData = { success: req.session.success };
    delete req.session.success;

    res.render('login', viewData);
});

router.post('/', function (req, res) {

    userRoute.signIn(req, res, function(error, token){
        if(error)
            return res.render('login', { error: 'An error occurred' });

        else if(token == "")
            return res.render('login', { error: 'Username or password is incorrect', username: req.body.username });

        else{
            req.session.token = token;
            res.redirect("/app");
        }

    });

});

module.exports = router;