var mongoose = require('mongoose');
var express = require('express');
var router = express.Router();
var request = require('request');
var crypto = require('crypto');

router.get('/getCurrentUser/:userId', function (req, res) {
    var userId = req.params.userId;
    var userModel = mongoose.model('users');
    userModel.findOne({userId: userId},function(err, user) {
        if(!err && user != null){
            res.send(user);
        }else{
            res.sendStatus(400);
        }
    })
});

router.post('/updateUser', function (req, res) {
    var userModel = mongoose.model('users');
    var userId = req.body.userId;
    var password = req.body.password;
    var age = req.body.age;

    var pwd =  crypto.createHash("sha1").update(password).digest(
        'hex');
    userModel.update({userId: userId}, {password: pwd, age: age},function (err, doc){
        if(!err){
            res.sendStatus(200);
        }else{
            res.sendStatus(400);
        }
    });
});


module.exports = router;