/**
 * Created by vaide_000 on 4/18/2016.
 */
var express = require('express');
var router = express.Router();
var request = require('request');

router.get('/', function (req, res) {
   res.render('about');
});

module.exports = router;