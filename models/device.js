var mongoose = require('mongoose');

module.exports = function DeviceModel(){
    var deviceSchema = mongoose.Schema({
        id : String,
        userId: String,
        name: String,
        deviceTopic: String,
        status : Number
    },{
        collection: 'deviceDetails'
    });

    return mongoose.model('device', deviceSchema);
}




