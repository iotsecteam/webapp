var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var crypto = require('crypto');

var usersSchema = new Schema({
	userId : {
		type : String,
		required : true,
		index : {
			unique : true
		}
	},
	password : {
		type : String,
		required : true
	},
	name : String,
	gender : String,
	age : Number
});

usersSchema.methods.encryptPassword = function() {

	this.password = crypto.createHash("sha1").update(this.password).digest(
			'hex');
	return this.password;
};
mongoose.model('users', usersSchema);