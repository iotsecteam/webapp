var chai = require("chai");
var expect = require("chai").expect;
var request = require("superagent");

describe('testSample', function(){
	it('returns lowercase of string', function(){
		expect('HELLO WORLD').to.equal('HELLO WORLD')
	});
})
	
	
	describe('testSample2', function(){
		it('returns uppercase of string', function(){
			expect('HELLO WORLD').to.equal('HELLO WORLD')
		})
	});
	/*describe("When requested /", function(){
		it("should return 200 status", function(done) {
			request.get("http://localhost:3000/").end(function assert(err, res){
				expect(err).to.not.be.ok;
				expect(res).to.have.property('status',200);
				done();
			})
		})
	})*/
