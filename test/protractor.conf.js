/**
 * Created by vaide_000 on 3/8/2016.
 */
exports.config={
    seleniumAddress: 'http://localhost:4444/wd/hub',

    capabilities: {
        'browserName' : 'chrome'
    },

    specs: [
        './e2e/**/*.spec.js'
    ],
    baseUrl: 'http://locathost:3000',

    onPrepare : function(){

    },

    jasmineNodeOpts: {
        showColors: true,
        defaultTimeOutInterval: 30000,

        isVerbose: true,
        includeStackTrace: true
    }
};