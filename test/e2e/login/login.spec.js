/**
 * Created by vaide_000 on 3/8/2016.
 */

describe('testSample', function(){

});

describe('Authentication capabilities', function() {
    var fail = function() { expect(true).toBe(false); }
    var loginURL;
    browser.driver.get('http://localhost:3000/login');
    browser.ignoreSynchronization = true;

    it('should redirect to the login page if trying to load protected page while not authenticated', function(){
        browser.driver.get('http://localhost:3000/login');
        loginURL = browser.driver.getCurrentUrl();
        console.log("loginURL"+loginURL);
        browser.driver.get('http://localhost:3000/#/');
        expect(browser.driver.getCurrentUrl()).toContain(loginURL);
    });

    it('should warn on missing/malformed credentials', function(){
        browser.driver.get('http://localhost:3000/login');
        /*userId.clear();
        password.clear();*/

        browser.driver.findElement(by.id('userId')).clear();
        browser.driver.findElement(by.id('password')).clear();
        browser.driver.findElement(by.id('loginButton')).click();
        expect(browser.driver.findElement(by.css('.alert-danger')).isDisplayed()).toBe(true);
        browser.driver.wait(function() {

            return true;
        });

        browser.driver.findElement(by.id('password')).sendKeys('test');
        browser.driver.findElement(by.id('loginButton')).click();
        //expect(error.getText()).toMatch('Username or password is incorrect');
        expect(browser.driver.findElement(by.css('.alert-danger')).isDisplayed()).toBeTruthy();

        browser.driver.findElement(by.id('userId')).sendKeys('test');
        browser.driver.findElement(by.id('loginButton')).click();
        expect(browser.driver.findElement(by.css('.alert-danger')).isDisplayed()).toBeTruthy();
        //expect(error.getText()).toMatch('Username or password is incorrect');

        browser.driver.findElement(by.id('userId')).sendKeys('test');
        browser.driver.findElement(by.id('password')).clear();
        browser.driver.findElement(by.id('loginButton')).click();
        expect(browser.driver.findElement(by.css('.alert-danger')).isDisplayed()).toBeTruthy();
        //expect(error.getText()).toMatch('Username or password is incorrect');
    });

    it('should accept a valid email address and password', function(){
        browser.driver.findElement(by.id('userId')).clear();
        browser.driver.findElement(by.id('password')).clear();

        browser.driver.findElement(by.id('userId')).sendKeys('swarad');
        browser.driver.findElement(by.id('password')).sendKeys('123');
        browser.driver.findElement(by.id('loginButton')).click();
        expect(browser.getCurrentUrl()).not.toEqual(loginURL);
    });

    it('should return to the login page after logout', function(){
        //var logoutButton = $('a.logout');
        browser.driver.findElement(by.id('logoutButton')).click();
        expect(browser.driver.getCurrentUrl()).toContain('http://localhost:3000/login');
    });
});